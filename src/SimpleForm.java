import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class SimpleForm extends Application {

	public static void main(String[] args) {
		launch(args);

	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// 1. Create & configure user interface controls

		Label namelabel1= new Label("Enter your name");
		TextField nameTextField1 = new TextField();
		

		Label namelabel2= new Label("Enter hours worked");
		TextField nameTextField2 = new TextField();
		

		Label namelabel3= new Label("Enter hourly rate");
		TextField nameTextField3 = new TextField();
		
		Label result= new Label("");
		
		
		Button goButton = new Button();
		goButton.setText("Calculte");
		goButton.setOnAction(new EventHandler<ActionEvent>() {
		    @Override
		    public void handle(ActionEvent e) {
		    	String namee= nameTextField1.getText();
		    	double hours = Double.valueOf(nameTextField2.getText());
		    	double rate = Double.valueOf(nameTextField3.getText());
		    	double wages= hours*rate;
		    	result.setText(namee+", Your wages are $"+wages);
		    	
		    	nameTextField1.setText("");
		    	nameTextField2.setText("");
		    	nameTextField3.setText("");
		    }
		});
		
		// 2. Make a layout manager
		VBox root = new VBox();

		// 3. Add controls to the layout manager
		root.getChildren().add(namelabel1);
		root.getChildren().add(nameTextField1);
		
		root.getChildren().add(namelabel2);
		root.getChildren().add(nameTextField2);
		root.getChildren().add(namelabel3);
		root.getChildren().add(nameTextField3);
		root.getChildren().add(goButton);
		root.getChildren().add(result);

		// 4. Add layout manager to scene
		// 5. Add scene to a stage
		
		primaryStage.setScene(new Scene(root,300,300)); // setting width and height to 1090,1080
		
        primaryStage.setTitle("Example 1 of GUI");
		// 6. Show the app
         primaryStage.show();
	}

}
